import os
import csv
import json
import time
import requests
import datetime
import random
import string
import urllib.parse
import uuid
import pymongo
from multiprocessing.dummy import Pool as ThreadPool
import concurrent.futures
import urllib.request
from random import shuffle
import spacy
import numpy as np
import pandas as pd
from bson.objectid import ObjectId
import pickle
import sys

import keras
from keras.preprocessing.text import Tokenizer, text_to_word_sequence
from keras.preprocessing.sequence import pad_sequences
from keras.models import load_model
from keras.preprocessing.image import img_to_array
from keras.applications import imagenet_utils
from keras.layers import Embedding
from keras.layers import Dense, Input, Flatten, Add
from keras.layers import Conv1D, MaxPooling1D, Embedding, Dropout, LSTM, GRU, Bidirectional, TimeDistributed
from keras.models import Model
from keras import backend as K
from keras.engine.topology import Layer, InputSpec
from sklearn.metrics import roc_auc_score
from keras.callbacks import Callback
from sklearn.preprocessing import LabelBinarizer
import usage_testing_nlp
import itertools
import matplotlib.pyplot as plt
import glob
from scipy.misc import imresize
import matplotlib.image as mpimg
from scipy import misc
from keras.applications.vgg16 import VGG16
from keras.layers import *
from keras.preprocessing.image import load_img, img_to_array
from skimage import transform
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.applications.vgg19 import VGG19

sys.path.append("scripts")
sys.path.append("scripts/vectorization")
sys.path.append("../scripts")
sys.path.append("../scripts/vectorization")

import get_image_vector3
from app import get_image_vector2
import get_image_vector1
# import get_deepfashion_vector
import get_unified_vector
import download_images

INCEPTION_MODEL_FILE = "scripts/vectorization/models/inception_gd.h5"
DEEPFASHION_MODEL_FILE = "scripts/vectorization/models/ir_deepfashion.h5"
RESNET_MODEL_FILE = "scripts/vectorization/models/resnet_gd.h5"
VGG19_MODEL_FILE = "scripts/vectorization/models/vgg19_gd.h5"
NLP_MODEL_FILE = "scripts/vectorization/models/model_12_128.h5"

def get_image_filepaths(posts):
    post_ids_filepaths = []
    ####DOWNLOAD FILE#####
    # Takes in a batch of posts and returns a list of tuples such as (post_id, filepath)
    post_ids_filepaths = download_images.download_images_return_id_location(posts)
    ####DOWNLOAD FILE#####
    return post_ids_filepaths
    pass


def vectorize_the_fuck(clean_post_list, inception_model, resnet_model, vgg19_model, nlp_model):
    post_ids_filepaths = get_image_filepaths(clean_post_list)
    for post_id_filepath in post_ids_filepaths:
        print(post_id_filepath[0]+" : "+post_id_filepath[1])

    # create a list of valid and invalid filepaths and send them to vectorization func acc. then concatenate the results and return
    valid_clean_post_list = []
    invalid_clean_post_list = []
    for post_id_filepath in post_ids_filepaths:
        id = post_id_filepath[0]
        filepath = post_id_filepath[1]
        if filepath == "":
            invalid_clean_post_list.append(id)
        else:
            valid_clean_post_list.append(id)

    valid_vector_post =  vectorize_the_fuck_with_valid_vectors(clean_post_list, valid_clean_post_list, inception_model, resnet_model, vgg19_model, nlp_model)
    return valid_vector_post

def vectorize_the_fuck_with_valid_vectors(clean_post_list, valid_id_list, inception_model, resnet_model, vgg19_model, nlp_model):
    vectorised_posts = []

    # deepfashion_1024_posts = get_deepfashion_vector.load_model_vectorise_deepfashion_1024(clean_post_list)
    inception_1024_posts = get_image_vector2.load_model_vectorise_inception_1024(clean_post_list, inception_model)
    resnet_1024_posts = get_image_vector1.load_model_vectorise_resnet_1024(clean_post_list, resnet_model)
    vgg19_1024_posts = get_image_vector3.load_model_vectorise_vgg19_1024(clean_post_list, vgg19_model)

    unified_512_posts = get_unified_vector.load_model_vectorise_unified_512(clean_post_list, nlp_model)


    for clean_post in clean_post_list:
        id = clean_post["_id"]
        clean_post["_id"] = ObjectId(id)
        clean_post["website_id"] = ObjectId(clean_post["website_id"])

        if id in valid_id_list:
            print(str(id))
            # clean_post["ir_vector"] = deepfashion_1024_posts[id]["ir_vector"]
            clean_post["nlp_vector"] = unified_512_posts[id]["nlp_vector"]
            clean_post["resnet_vector"] = resnet_1024_posts[id]["resnet_vector"]
            clean_post["inceptionv3_vector"] = inception_1024_posts[id]["inceptionv3_vector"]
            clean_post["vgg19_vector"] = vgg19_1024_posts[id]["vgg19_vector"]

            # print("ir_vector: "+str(len(clean_post["ir_vector"])))
            # print("nlp_vector: "+str(len(clean_post["nlp_vector"])))
            # print("resnet_vector: "+str(len(clean_post["resnet_vector"])))
            # print("inceptionv3_vector: "+str(len(clean_post["inceptionv3_vector"])))
            # print("vgg19_vector: "+str(len(clean_post["vgg19_vector"])))

            vectorised_posts.append(clean_post)
        else:
            print(str(id)+": empty vectors added")
            # clean_post["ir_vector"] = deepfashion_1024_posts[id]["ir_vector"]
            clean_post["nlp_vector"] = [0]*512
            clean_post["resnet_vector"] = [0]*1024
            clean_post["inceptionv3_vector"] = [0]*1024
            clean_post["vgg19_vector"] = [0]*1024

            vectorised_posts.append(clean_post)

    return vectorised_posts

def vectorize_the_fuck_with_empty_vectors(clean_post_list):
    vectorised_posts = []
    for clean_post in clean_post_list:
        id = clean_post["_id"]
        print(str(id)+": empty vectors added")

        # clean_post["ir_vector"] = deepfashion_1024_posts[id]["ir_vector"]
        clean_post["nlp_vector"] = [0]*512
        clean_post["resnet_vector"] = [0]*1024
        clean_post["inceptionv3_vector"] = [0]*1024
        clean_post["vgg19_vector"] = [0]*1024

        vectorised_posts.append(clean_post)

    return vectorised_posts

def clean_delete_key(post, key):
    try:
        del post[key]
    except Exception as e:
        pass

def cleanse_post(post):
    post["_id"] = str(post["_id"]).replace("ObjectId(\"","").replace("\")","")
    try:
        post["website_id"] = str(post["website_id"]).replace("ObjectId(\"","").replace("\")","")
    except Exception as e:
        pass
    try:
        image_url = post["media"]["standard"][0]["url"]
    except Exception as e:
        image_url = ""

    clean_delete_key(post, "media")
    post["media"] = {"standard":[{"order":1,"url":image_url}]}

    clean_delete_key(post, "created_at")
    clean_delete_key(post, "updated_at")
    clean_delete_key(post, "attributes")
    clean_delete_key(post, "gallery")
    clean_delete_key(post, "meta")
    clean_delete_key(post, "sku")
    clean_delete_key(post, "sizes")
    clean_delete_key(post, "units")
    clean_delete_key(post, "related_products")
    clean_delete_key(post, "breadcrumbs")
    try:
        post["description_text"] = post["description"]
        clean_delete_key(post, "description")
    except Exception as e:
        pass
    if "description_text" in post:
        pass
    else:
        post["description_text"] = ""
    if "name" in post:
        pass
    else:
        post["name"] = ""
    return post

if __name__ == "__main__":

    vgg19_model = get_image_vector3.deep_rank_model()
    vgg19_model.load_weights(VGG19_MODEL_FILE)

    inception_model = get_image_vector2.deep_rank_model()
    inception_model.load_weights(INCEPTION_MODEL_FILE)

    resnet_model = get_image_vector1.deep_rank_model()
    resnet_model.load_weights(RESNET_MODEL_FILE)

    nlp_model = get_unified_vector.get_unified_model(NLP_MODEL_FILE)

    WEBSITE_ID_HASH = {}
    WEBSITE_ID_HASH["queens_cz"] = "5bf39a8bc9a7f60004dd8d04"
    WEBSITE_ID_HASH["zoot_cz"] = "5bf39a6fc9a7f60004dd8d03"
    WEBSITE_ID_HASH["footshop_cz"] = "5bf39aa9c9a7f60004dd8d05"
    WEBSITE_ID_HASH["answear_cz"] = "5bf399f7c9a7f60004dd8d01"
    WEBSITE_ID_HASH["aboutyou_cz"] = "5bf399d3c9a7f60004dd8d00"
    # WEBSITE_ID_HASH["zalando_cz"] = "5bf39a55c9a7f60004dd8d02"
    WEBSITE_ID_HASH["freshlabels_cz"] = "5c38cbea0359f800041cdab0"

    WEBSITE_ID_HASH["hervis_at"] = "5ba20a59c423de434b232c36"
    WEBSITE_ID_HASH["xxlsports_at"] = "5ba20a93c423de434b232c37"
    WEBSITE_ID_HASH["decathlon_at"] = "5ba20abdc423de434b232c38"
    WEBSITE_ID_HASH["gigasport_at"] = "5ba20ae2c423de434b232c39"
    WEBSITE_ID_HASH["bergzeit_at"] = "5ba20b02c423de434b232c3a"
    WEBSITE_ID_HASH["blue_tomato_at"] = "5ba20b2cc423de434b232c3b"

    WEBSITE_ID_HASH["asos_gb"] = "5bc055046264490004432328"
    WEBSITE_ID_HASH["debenhams_gb"] = "5c3cae7c29ecc300044c5f67"
    WEBSITE_ID_HASH["marksandspencer_gb"] = "5c3caf2429ecc300044c5f69"

    website_id_hash = WEBSITE_ID_HASH


    MONGODB_URL1 = 'mongodb://admin:epxNOGMHaAiRRV5q@mongodb-prod.greendeck.co:27017/admin'
    client = pymongo.MongoClient(
        MONGODB_URL1,
        ssl=False
    )
    db = client.octopus_fashion
    collection = db.combined_products

    MONGODB_URL2 = 'mongodb://admin:epxNOGMHaAiRRV5q@mongodb-prod.greendeck.co:27017/admin'
    client_sink = pymongo.MongoClient(
        MONGODB_URL2,
        ssl=False
    )
    db_sink = client_sink.faissal
    collection_sink = db_sink.combined_products_vec


    for key, website_id in website_id_hash.items():
        new_posts = []
        print(key)
        try:
            cc = collection.find({"website_id": ObjectId(website_id)})
            for post in list(cc):
                post = cleanse_post(post)
                new_posts.append(post)
            client.close()
        except Exception as e:
            raise

        for batch in range(0, len(new_posts), 100):
            print(str(len(new_posts)))
            vectorized_posts = vectorize_the_fuck(new_posts[batch:batch+100], inception_model, resnet_model, vgg19_model, nlp_model)
            for vectorized_post in vectorized_posts:
                collection_sink.insert_one(vectorized_post)
            print("combined_products_vec: "+str(collection_sink.count_documents({})))
